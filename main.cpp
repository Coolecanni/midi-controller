#include <Arduino.h>
#include <MIDI.h>

static const unsigned sUsbTransportBufferSize = 16;

MIDI_CREATE_DEFAULT_INSTANCE();

//POTMETERS//

const int pot1 = A0;

int potVal1 = 0;

int lastPotVal1 = 0;

//BUTTONS//

const int but1 = 2;

int butVal1 = 0;

int lastButVal1 =0;

void setup() { 
  
  pinMode(but1, INPUT_PULLUP); // Push Button with pull-up
  MIDI.begin(MIDI_CHANNEL_OMNI);
  Serial.begin(115200);
}
/*
void MIDImessage(byte status, byte data1, byte data2)
{
  Serial.write(status);
  Serial.write(data1);
  Serial.write(data2);
}
*/

void readPots() { 
    
    int diff = 4;

    potVal1 = analogRead(pot1);

    int potVal1diff = potVal1 - lastPotVal1;

      if (abs(potVal1diff) > diff)                              // execute only if new and old values differ enough
  {
        MIDI.sendNoteOn(22, map(potVal1, 0, 1023, 0, 127), 2);     // map sensor range to MIDI rang
         lastPotVal1 = potVal1;                                 // reset old value with new reading
  }

delay(15);

}

void readButtons() {

    butVal1 = digitalRead(but1);

    if (butVal1 != lastButVal1)
    {
      if (butVal1 == HIGH)
      {
        MIDI.sendNoteOn(21, 127, 1);        
      }

    else
    {
      //MIDI.sendNoteOff(21, 1, 0);
    }
    
    delay(500);

    }

  lastButVal1 = butVal1;  

}

void loop() {

  readPots();    
  readButtons();

}
