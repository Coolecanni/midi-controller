#include <Arduino.h>
#include <MIDI.h>
#include <midi_Message.h>

static const unsigned sUsbTransportBufferSize = 16;

MIDI_CREATE_DEFAULT_INSTANCE();

//POTMETERS//

const int pot1 = A0;
const int pot2 = A1;
const int pot3 = A2;
const int pot4 = A3;
const int pot5 = A4;

int potVal1 = 0;
int potVal2 = 0;
int potVal3 = 0;
int potVal4 = 0;
int potVal5 = 0;

int lastPotVal1 = 0;
int lastPotVal2 = 0;
int lastPotVal3 = 0;
int lastPotVal4 = 0;
int lastPotVal5 = 0;

//BUTTONS//

const int but1 = 2;
const int but2 = 3;
const int but3 = 4;
const int but4 = 5;
const int but5 = 6;
const int but6 = 7;
const int but7 = 8;
const int but8 = 9;
const int but9 = 10;
const int but10 = 11;

int butVal1 = 0;
int butVal2 = 0;
int butVal3 = 0;
int butVal4 = 0;
int butVal5 = 0;
int butVal6 = 0;
int butVal7 = 0;
int butVal8 = 0;
int butVal9 = 0;
int butVal10 = 0;

int lastButVal1 =0;
int lastButVal2 =0;
int lastButVal3 =0;
int lastButVal4 =0;
int lastButVal5 =0;
int lastButVal6 =0;
int lastButVal7 =0;
int lastButVal8 =0;
int lastButVal9 =0;
int lastButVal10 =0;

void setup() { 
  
  pinMode(but1, INPUT_PULLUP);                                  // Push Button with pull-up
  MIDI.begin(MIDI_CHANNEL_OMNI);
  Serial.begin(115200);
}

void readPots() { 
    
    int diff = 8;

    potVal1 = analogRead(pot1);
    potVal2 = analogRead(pot2);
    potVal3 = analogRead(pot3);
    potVal4 = analogRead(pot4);
    potVal5 = analogRead(pot5);

    int potVal1diff = potVal1 - lastPotVal1;
    int potVal2diff = potVal2 - lastPotVal2;
    int potVal3diff = potVal3 - lastPotVal3;
    int potVal4diff = potVal4 - lastPotVal4;
    int potVal5diff = potVal5 - lastPotVal5;

    int vPot1 = map(potVal1, 0, 1023, 127, 0);                  // map sensor range to MIDI range
    int vPot2 = map(potVal2, 0, 1023, 127, 0);                  // map sensor range to MIDI range
    int vPot3 = map(potVal3, 0, 1023, 127, 0);                  // map sensor range to MIDI range
    int vPot4 = map(potVal4, 0, 1023, 127, 0);                  // map sensor range to MIDI range
    int vPot5 = map(potVal5, 0, 1023, 127, 0);                  // map sensor range to MIDI range

      if (abs(potVal1diff) > diff)                              // execute only if new and old values differ enough
  {
    MIDI.sendNoteOn(1, vPot1, 2);
    lastPotVal1 = potVal1;                                      // reset old value with new reading
  }
      if (abs(potVal2diff) > diff) 
  {
    MIDI.sendNoteOn(2, vPot2, 2);
    lastPotVal2 = potVal2;                                      // reset old value with new reading
  }

        if (abs(potVal3diff) > diff) 
  {
    MIDI.sendNoteOn(3, vPot3, 2);
    lastPotVal3 = potVal3;                                      // reset old value with new reading
  }

      if (abs(potVal4diff) > diff) 
  {
    MIDI.sendNoteOn(4, vPot4, 2);
    lastPotVal4 = potVal4;                                      // reset old value with new reading
  }

      if (abs(potVal5diff) > diff) 
  {
    MIDI.sendNoteOn(5, vPot5, 2);
    lastPotVal5 = potVal5;                                      // reset old value with new reading
  }


delay(15);

}

//BUTTONS//

void readButtons() {

    butVal1 = digitalRead(but1);

    if (butVal1 != lastButVal1)
    {
      if (butVal1 == HIGH)
      {
        MIDI.sendNoteOn(21, 127, 1);        
      }

      else
      {
        MIDI.sendNoteOff(21, 0, 1);
      }
    
    delay(15);

    }

  lastButVal1 = butVal1;  

    butVal2 = digitalRead(but2);

    if (butVal2 != lastButVal2)
    {
      if (butVal2 == HIGH)
      {
        MIDI.sendNoteOn(22, 127, 1);        
      }

      else
      {
        MIDI.sendNoteOff(22, 0, 1);
      }
    
    delay(15);

    }

  lastButVal2 = butVal2;

    butVal3 = digitalRead(but3);

    if (butVal3 != lastButVal3)
    {
      if (butVal3 == HIGH)
      {
        MIDI.sendNoteOn(23, 127, 1);        
      }

      else
      {
        MIDI.sendNoteOff(23, 0, 1);
      }
    
    delay(15);

    }

  lastButVal3 = butVal3;

      butVal4 = digitalRead(but4);

    if (butVal4 != lastButVal4)
    {
      if (butVal4 == HIGH)
      {
        MIDI.sendNoteOn(24, 127, 1);        
      }

      else
      {
        MIDI.sendNoteOff(24, 0, 1);
      }
    
    delay(15);

    }

  lastButVal4 = butVal4;

    butVal5 = digitalRead(but5);

    if (butVal5 != lastButVal5)
    {
      if (butVal5 == HIGH)
      {
        MIDI.sendNoteOn(25, 127, 1);        
      }

      else
      {
        MIDI.sendNoteOff(25, 0, 1);
      }
    
    delay(15);

    }

  lastButVal5 = butVal5;

    butVal6 = digitalRead(but6);

    if (butVal6 != lastButVal6)
    {
      if (butVal6 == HIGH)
      {
        MIDI.sendNoteOn(26, 127, 1);        
      }

      else
      {
        MIDI.sendNoteOff(26, 0, 1);
      }
    
    delay(15);

    }

  lastButVal6 = butVal6;

    butVal7 = digitalRead(but7);

    if (butVal7 != lastButVal7)
    {
      if (butVal7 == HIGH)
      {
        MIDI.sendNoteOn(27, 127, 1);        
      }

      else
      {
        MIDI.sendNoteOff(27, 0, 1);
      }
    
    delay(15);

    }

  lastButVal7 = butVal7;

    butVal8 = digitalRead(but8);

    if (butVal8 != lastButVal8)
    {
      if (butVal8 == HIGH)
      {
        MIDI.sendNoteOn(28, 127, 1);        
      }

      else
      {
        MIDI.sendNoteOff(28, 0, 1);
      }
    
    delay(15);

    }

  lastButVal8 = butVal8;

    butVal9 = digitalRead(but9);

    if (butVal9 != lastButVal9)
    {
      if (butVal9 == HIGH)
      {
        MIDI.sendNoteOn(29, 127, 1);        
      }

      else
      {
        MIDI.sendNoteOff(29, 0, 1);
      }
    
    delay(15);

    }

  lastButVal9 = butVal9;

    butVal10 = digitalRead(but10);

    if (butVal10 != lastButVal10)
    {
      if (butVal10 == HIGH)
      {
        MIDI.sendNoteOn(30, 127, 1);        
      }

      else
      {
        MIDI.sendNoteOff(30, 0, 1);
      }
    
    delay(15);

    }

  lastButVal10 = butVal10;

}

void loop() {

  readPots();    
  readButtons();

}
