#include <Arduino.h>
#include <MIDI.h>
#include <midi_Message.h>

static const unsigned sUsbTransportBufferSize = 16;

MIDI_CREATE_DEFAULT_INSTANCE();

//POTMETERS//

const int pot1 = A0;
const int pot2 = A1;

int potVal1 = 0;
int potVal2 = 0;

int lastPotVal1 = 0;
int lastPotVal2 = 0;

//BUTTONS//

const int but1 = 2;

int butVal1 = 0;

int lastButVal1 =0;

void setup() { 
  
  pinMode(but1, INPUT_PULLUP);                                  // Push Button with pull-up
  MIDI.begin(MIDI_CHANNEL_OMNI);
  Serial.begin(115200);
}

void readPots() { 
    
    int diff = 4;

    potVal1 = analogRead(pot1);
    potVal2 = analogRead(pot2);

    int potVal1diff = potVal1 - lastPotVal1;
    int potVal2diff = potVal2 - lastPotVal2;

    int vPot1 = map(potVal1, 0, 1023, 0, 127);                  // map sensor range to MIDI range
    int vPot2 = map(potVal2, 0, 1023, 0, 127);                  // map sensor range to MIDI range

      if (abs(potVal1diff) > diff)                              // execute only if new and old values differ enough
  {
    MIDI.sendControlChange(1, vPot1, 2);
    lastPotVal1 = potVal1;                                      // reset old value with new reading
  }
      if (abs(potVal2diff) > diff) 
  {
    MIDI.sendControlChange(2, vPot2, 2);
    lastPotVal2 = potVal2;                                      // reset old value with new reading
  }

delay(15);

}

//BUTTONS//

void readButtons() {

    butVal1 = digitalRead(but1);

    if (butVal1 != lastButVal1)
    {
      if (butVal1 == HIGH)
      {
        MIDI.sendNoteOn(21, 127, 1);        
      }

      else
      {
        MIDI.sendNoteOff(21, 0, 1);
      }
    
    delay(15);

    }

  lastButVal1 = butVal1;  

}

void loop() {

  readPots();    
  readButtons();

}
